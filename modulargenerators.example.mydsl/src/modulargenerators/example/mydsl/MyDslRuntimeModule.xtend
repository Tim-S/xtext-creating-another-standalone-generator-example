/*
 * generated by Xtext 2.14.0
 */
package modulargenerators.example.mydsl


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class MyDslRuntimeModule extends AbstractMyDslRuntimeModule {
}
