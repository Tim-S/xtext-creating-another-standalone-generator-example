package another.generator

import modulargenerators.example.mydsl.myDsl.Greeting
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGenerator2
import org.eclipse.xtext.generator.IGeneratorContext

class AnotherGenerator implements IGenerator2{

	override afterGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {}
	
	override beforeGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {}
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		fsa.generateFile("anotherFileGenerated.txt",'Greetings in Uppercase Format: ' + 
			resource.allContents
				.filter(Greeting)
				.map[name.toUpperCase]
				.join(', '))
	}
	
}