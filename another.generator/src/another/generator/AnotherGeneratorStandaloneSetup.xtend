package another.generator

import com.google.inject.Guice
import modulargenerators.example.mydsl.MyDslRuntimeModule
import modulargenerators.example.mydsl.MyDslStandaloneSetup

class AnotherGeneratorStandaloneSetup extends MyDslStandaloneSetup {
	override createInjector() {
		Guice.createInjector(new MyDslRuntimeModule() {
			override bindIGenerator2() {
				AnotherGenerator
			}
		});
	}
}