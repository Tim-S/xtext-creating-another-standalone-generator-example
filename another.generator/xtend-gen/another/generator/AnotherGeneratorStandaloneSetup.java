package another.generator;

import another.generator.AnotherGenerator;
import com.google.inject.Guice;
import com.google.inject.Injector;
import modulargenerators.example.mydsl.MyDslRuntimeModule;
import modulargenerators.example.mydsl.MyDslStandaloneSetup;
import org.eclipse.xtext.generator.IGenerator2;

@SuppressWarnings("all")
public class AnotherGeneratorStandaloneSetup extends MyDslStandaloneSetup {
  @Override
  public Injector createInjector() {
    return Guice.createInjector(new MyDslRuntimeModule() {
      @Override
      public Class<? extends IGenerator2> bindIGenerator2() {
        return AnotherGenerator.class;
      }
    });
  }
}
