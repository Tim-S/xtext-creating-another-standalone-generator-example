package another.generator;

import com.google.common.collect.Iterators;
import modulargenerators.example.mydsl.myDsl.Greeting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGenerator2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class AnotherGenerator implements IGenerator2 {
  @Override
  public void afterGenerate(final Resource input, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
  }
  
  @Override
  public void beforeGenerate(final Resource input, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
  }
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    final Function1<Greeting, String> _function = (Greeting it) -> {
      return it.getName().toUpperCase();
    };
    String _join = IteratorExtensions.join(IteratorExtensions.<Greeting, String>map(Iterators.<Greeting>filter(resource.getAllContents(), Greeting.class), _function), ", ");
    String _plus = ("Greetings in Uppercase Format: " + _join);
    fsa.generateFile("anotherFileGenerated.txt", _plus);
  }
}
